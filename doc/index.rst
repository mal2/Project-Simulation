.. Ising 2D Simulation documentation master file, created by
   sphinx-quickstart on Wed Dec 16 16:45:06 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Project documentation!
===============================================

DBScan
==================
.. automodule:: dbscan
.. autoclass:: Dbscan
   :members:
.. automodule:: nearestneighbour
   :members:
.. automodule:: plotting
   :members:

Ising
==================
.. automodule:: System
.. autoclass:: Init
   :members:
   :special-members:
.. autoclass:: Metropolis
   :members:
.. autoclass:: Wolff
   :members:
.. automodule:: Plot
.. autoclass:: Show
   :members:

PCA
==================
.. automodule:: pca
   :members:
.. automodule:: pcaGpu
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

