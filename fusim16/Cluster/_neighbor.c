#include <Python.h>
#include <stdio.h>

double dist;

double _distancesquared (int dim, double* x, double* p)
{
        int i;
        dist = 0.0;
        for (i=0; i<dim; i++)
        {
                        dist = dist + (x[i] - p[i])*(x[i] - p[i]);
        }
        return dist;
}

void _getNeighbors (int dim, int numberOfEntries, int* neighbors, double* data, double* p, double* epssquared)
{
        int i,j;
        int neighborCt=0;
        for (j=0; j<numberOfEntries; j++)
        {
                        dist = 0.0;
                        

                        for (i=0; i<dim; i++)
                        {
                                int l = j + i*numberOfEntries;
                                dist = dist + (data[l] - p[i])*(data[l] - p[i]);

                              //printf("%fl", data[l]);

                        }
                        //printf("\n");


                        if (dist <= epssquared[0])
                        {
                                neighborCt = neighborCt+1;
                                neighbors[neighborCt] = j;
                        }

        }
        neighbors[0] = neighborCt;

}