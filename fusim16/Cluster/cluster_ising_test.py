import numpy as np
import dbscan
#from pca import pca
def fromFile(filename, length):
        with open(str(filename),'r') as f_handle:
            x = np.fromfile(f_handle, dtype=bool)
        system = np.reshape(x,(-1,length*length))
        return system


x = fromFile("out_wo_header_16_2000.bin",16)
print x.shape
minNeighbors = 30
epsilon = 1.01


data = np.array(x, dtype=float)
dbscanner = dbscan.Dbscan(data, minNeighbors)
dbscanner.run()
clusterList=dbscanner.getClusterList()
sum = 0
for cluster in clusterList:
    print  np.array(cluster).shape[0]
    sum = np.array(cluster).shape[0] + sum

print sum
print np.array(dbscanner.getNoise()).shape