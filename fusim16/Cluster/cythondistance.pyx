cimport numpy

cdef extern from "_neighbor.h":
    double _distancesquared (int dim, double* x, double* p)

cdef extern from "_neighbor.h":
    void _getNeighbors (int dim, int numberOfEntries, int* neighbors, double* data, double* p, double* epssquared)


def distancesquared(dim, x, p):
    """
    Calculates the squared distance. x and p have to be numpy arrays of double with the same lenght that is given by dim for this to work
    :param dim:
        the dimension of x AND p
    :param x:
       first point
    :param p:
        second point
    :return: double distance
        square of the distance of both points
    """
    _x = <double*> numpy.PyArray_DATA(x)
    _p = <double*> numpy.PyArray_DATA(p)
    return _distancesquared(dim, _x, _p)

def getNeighbors(dim, numberOfEntries, neighbors, data, p, epssquared):
    _neighbors = <int*> numpy.PyArray_DATA(neighbors)
    _data = <double*> numpy.PyArray_DATA(data)
    _p = <double*> numpy.PyArray_DATA(p)
    _epssquared = <double*> numpy.PyArray_DATA(epssquared)
    _getNeighbors(dim, numberOfEntries, _neighbors, _data, _p, _epssquared)
