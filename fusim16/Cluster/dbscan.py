__author__ = 'arno'

import numpy as np
from nearestneighbour import Neighbor
import nearestneighbour

class Dbscan(object):
    """
    :param D: Eine :math:`\mathbb{R}^{N x D}` Matrix des Typs np.array. Wobei :math:`N =` Anzahl der Datenpunkte und :math:`D =` Anzahl der Dimeonsionen pro Punkt
    :param eps: Der Radius, welcher die Nachbarschaft definiert. Im Bild weiter unten veranschaulicht durch den Radius um jeden Punkt. :math:`\epsilon` wird mit dem euklidischem Abstand verglichen. Falls kein :math:`\epsilon` definiert wurde ,wird automatisch der zweifache durschnitlliche Abstand von jedem Punkt in D zu seinem naehstem Nachbarn als :math:`\epsilon` festgelegt (Achtung dauert sehr lange).
    :param minPtsEin: Mindestanzahl an Nachbarn eines Punktes,welche benoetigt wird um als Cluster klassifiziert zu werden.
    """
    def __init__(self,D,minPts,eps=None):

        self.D=D
        self.visitedPoints=np.zeros(len(D),dtype=bool)
        self.clusterByIndex=np.zeros(len(D), dtype=int)
        self.clusterCount=0
        self.eps=0.0

        if eps is None:
            eps=nearestneighbour.estimate_eps(D)*2
            print 'epsilon has been set to %d' % eps
            self.eps=eps
        else:
            self.eps=eps

        self.minPts=minPts



    def run(self):
        """

        .. image:: ../fusim16/Cluster/final_presentation/clustertheorie.png
           :height: 250px
           :width: 400 px
           :alt: alternate text
           :align: right


        Es wird der DBSCAN-Algorithmus ausgefuehrt. Abhaengig von den eingefuegten Parametern wird der Klasse dbscan ein Vektor self.clusterByIndex (mit dem Typ: np.array (:math:`\mathbb{N}^N`)) zugefuegt. Mit :math:`N =` Anzahl der Datenpunkte.

        - Dabei stehen Nullen innerhalb des Vektors fuer Punkte die dem Noise zugerechnet werden (siehe Punkt N im Bild).

        Weitere Nummern entsprechen dem jeweiligem Cluster, dem der Punkt zugeordnet wurde. Wie Ausserdem durch das Bild veranschaulicht werden soll,gibt es zwei weitere Arten von Objekten.

        - Kernobjekte, welche selbst dicht sind.

        - Dichte-erreichbare Objekte. Dies sind Objekte, die zwar von einem Kernobjekt des Clusters erreicht werden koennen, selbst aber nicht dicht sind. Anschaulich bilden diese den Rand eines Clusters. Diese koennen auch nicht eindeutig  immer den gleichen Clustern zugeordnet werden.



        """

        print "start dbscan with episilon = %d and minPts = %d" % (self.eps, self.minPts)
        near = Neighbor(self.D, self.eps)
        for i in range(len(self.D)):
            if not self.visitedPoints[i]:
                self.visitedPoints[i]= True
                #select the neighbors by the parameter eps
                NeighborIndex = near.nnPca(self.D[i])
                #if not enough neigbors are found, the point is "noise", else proceed
                if len(NeighborIndex) > (self.minPts):
                    #create a new cluster and put the point in it
                    self.clusterCount = self.clusterCount + 1
                    self.clusterByIndex[i] = self.clusterCount

                    #now check every neighbor p' of p
                    NGotBigger = True

                    while NGotBigger:
                        NGotBigger = False
                        for indexOfPoint in NeighborIndex:
                            if not self.visitedPoints[indexOfPoint]:
                                self.visitedPoints[indexOfPoint] = True
                                NeighborPrimeIndex = near.nnPca(self.D[indexOfPoint])
                                #if the point is not noise, then merge both neighborhoods
                                if len(NeighborPrimeIndex) > (self.minPts):
                                    NeighborIndex = NeighborIndex.union(NeighborPrimeIndex)
                                    NGotBigger = True
                            #if the point does not already belong to a cluster, put it this one
                            if self.clusterByIndex[indexOfPoint] == 0:
                                self.clusterByIndex[indexOfPoint]= self.clusterCount

        #print "Cluster List:"
        #for cl in clusterList:
        #    print cl
        #noise = self.filterNoise(clusterList, noiseOrReachable)
        #print "Noise:"
        #print noise

    def getClusterByIndex(self):
        """
        Gibt den Vektor clusterByIndex wieder.

        :return: self.clusterByIndex: Der Vektor, der die Information enthaelt, welche Punkt welchem Cluster bzw. Noise zugeordent wurde.
        """
        return self.clusterByIndex

    def getNoise(self):
        """
        Gibt den Vektor noise wieder.

        :return: noise: Der Vektor, der die Information enthaelt, welche Punkt welchem Noise zugeordent wurde. Entspricht allen NUllen bei ClusterByIndex.
        """
        noise = []
        for i in range(len(self.D)):
            if self.clusterByIndex[i]== 0:
                noise.append(self.D[i])
        return np.array(noise)

    def getClusterList(self):
        """

        :return: Clusterlist: Ein  Vektor des Typs np.array. Jedes Element des Vektors entspricht einer Liste, welche alle Punkte eines bestimmten Clusters enthaelt. Die Laenge des Vektors entspricht der Anzahl der CLuster.

        """

        Clusterlist=[]

        for Clusterindex in range(1,self.clusterCount+1):
            Clusteri=[]
            Clusterlist.append(Clusteri)

        for i in range(len(self.D)):
            whichClusterDoesPointIbelongTo = self.clusterByIndex[i]
            point =self.D[i]
            if whichClusterDoesPointIbelongTo != 0:
                Clusterlist[whichClusterDoesPointIbelongTo-1].append(point)

        return np.array(Clusterlist)




