import numpy
import cneighbor

__author__ = 'janek'

class Neighbor(object):
    """
    :param D: Eine :math:`\mathbb{R}^{N x D}` Matrix des Typs np.array. Wobei :math:`N =` die Anzahl der Datenpunkte ist und :math:`D =` Anzahl der Dimensionen pro Punkt.
    :param: :math:`\epsilon`: Der Radius, welcher die Nachbarschaft definiert.

    """

    def __init__(self, d, eps):
        self.d = d
        self.epssquare = eps * eps
        self.dFlat = numpy.ndarray.flatten(d, order='f')

    def nnPca(self, p):
        """
        Gibt die Nachbarn eines Punktes P in der Umgebung :math:`\epsilon` aus.

        :param: p: Der Punkt im np.array dessen Nachbarn ermittelt werden sollen

        :return: neighbors: Ein Set mit Nachbarn, des Punktes p im Datensatz D.
        """
        # neighbors ist eine Liste mit den Nachbarn, die innerhalb von eps liegen

        neighbors = numpy.zeros(len(self.d)+1, dtype=numpy.uint32)
        #for i in range(len(self.d)):
        #    if self.isNeighbor(p, p.shape[0], self.d[i], self.epssquare):
        #        neighbors.add(i)
        #neighbors = numpy.zeros(len(self.d), dtype=int)
        cneighbor.getNeighbors(p.shape[0], len(self.d), neighbors, self.dFlat, numpy.array(p, dtype=numpy.float64), numpy.array(self.epssquare))
        neighbors = neighbors[1:neighbors[0]+1]
        return set(neighbors)

def estimate_eps(D):
    """
    Wird nur bei fehlender Eingabe von :math:`\epsilon` aktiviert.

    :param D: Eine :math:`\mathbb{R}^{N x D}` Matrix des Typs np.array. Wobei :math:`N =` Anzahl der Datenpunkte und :math:`D =` Anzahl der Dimeonsionen pro Punkt

    :return: nearest_distance.mean(): Durschnitllicher Abstand von jedem Punkt in D zu seinem naehstem Nachbarn.

    """
    nearest_distance = []

    for i in range(len(D)):
        d= float("inf")
        for ii in range(len(D)):
            #print len(D)
            if i != ii:
                #dneu=(abs(D[i]-D[ii]))
                #print dneu
                #dneu=numpy.linalg.norm(dneu)

                #print dneu
                dneu = cneighbor.distancesquared(numpy.array(D[0]).size, numpy.array(D[i]), numpy.array(D[ii]))
                if dneu<d and dneu!=0:
                    d=dneu
        nearest_distance.append(d)
    #print nearest_distance
    nearest_distance=numpy.array(nearest_distance)
    return numpy.sqrt(nearest_distance.mean())