__author__ = 'janek'
import matplotlib.pyplot as plt
import numpy as np

def plotting(clusterList, noise):
    """

    Diese Methode funktioniert nur bei einer 2 dimensinalen Datenmatrix D: Eine :math:`\mathbb{R}^{N x 2}`

    :param: clusterList: Ein  Vektor des Typs np.array. Jedes Element des Vektors entspricht einer Liste, welche alle Punkte eines bestimmten Clusters enthaelt. Die Laenge des Vektors entspricht der Anzahl der CLuster.

    :param: noise: Der Vektor, der die Information enthaelt, welche Punkt welchem Noise zugeordent wurde. Entspricht allen Nullen bei ClusterByIndex.

    :return: Eine Visuallisierun der geclusterten zwei dimensionalen Daten.


    .. image:: ../fusim16/Cluster/final_presentation/2dimensionalcluster.png

    """
    #plot some data
    int=-1
    xclusterneu={}
    yclusterneu={}
    for i in range(len(clusterList)):
        cluster = clusterList[i]
        int+=1
        x_cluster=[]
        y_cluster=[]
        for point in cluster:
            x_cluster.append(point[0])
            y_cluster.append(point[1])
            xclusterneu[int]=[x_cluster]
            yclusterneu[int]=[y_cluster]


    datennoise=noise
    x_clusternoise=[]
    y_clusternoise=[]
    for item in datennoise:
        x_clusternoise.append(item[0])
        y_clusternoise.append(item[1])

    plt.plot(x_clusternoise,y_clusternoise,'mx')
    colours=['ro','bo','go','ko','co','wo','bo','ro','go','bo','ko','co','wo','bo','ro','go','bo','ko','co','wo','bo','ro','go','bo','ko','co','wo','bo']
    for plotindex in range(len(xclusterneu)):
        plt.plot(xclusterneu[plotindex],yclusterneu[plotindex],colours[plotindex])
    plt.xlabel('x')
    plt.ylabel('y')
    plt.show()