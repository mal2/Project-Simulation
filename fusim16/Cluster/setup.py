from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules=[
    Extension("cneighbor",
              sources=["./cythondistance.pyx" ,
                       "./_neighbor.c"],
              libraries=["m"] # Unix-like specific
    )
]

setup(
  name = "cNeighbor",
  ext_modules = ext_modules,
  cmdclass = {'build_ext': build_ext}
)