__author__ = 'janek'

import numpy as np
from dbscan import Dbscan
import unittest
from numpy import array
from numpy import loadtxt
from nearestneighbour import Neighbor
import plotting
import nearestneighbour

class TestClass(unittest.TestCase):

    #@unittest.skip("test_dbscan1")
    def test_dbscan1(self):
        testdata='testdata/eps2-minpts3-cluster1-noise0.dat'
        number, x_coordinate, y_coordinate      = np.loadtxt(testdata, unpack = True)
        D=[None]*len(x_coordinate)
        for ii in range(len(x_coordinate)):
            D[ii]=[x_coordinate[ii],y_coordinate[ii]]
        D=np.array(D)
        epsilon=4.0
        minNeighbors=3
        dbscanner = Dbscan(D, minNeighbors, epsilon)
        dbscanner.run()
        datennoise=dbscanner.getNoise()
        Dresultall=[]
        for daten in dbscanner.getClusterList():
            Dresult=[]
            for item in daten:
                Dresult.append(item)
            Dresultall.append(Dresult)  #
        #assert D==a
        Dr=np.array(Dresultall)
        a=np.array([])
        np.testing.assert_array_equal(a,datennoise)
        #assert datennoise==a
        #assert D.all()==Dr.all()

    #@unittest.skip("test_dbscan2")
    def test_dbscan2(self):
        testdata2='testdata/eps0p01-minpts1-cluster0-noise100.dat'
        number, x_coordinate, y_coordinate      = np.loadtxt(testdata2, unpack = True)
        D=[None]*len(x_coordinate)
        for ii in range(len(x_coordinate)):
            D[ii]=[x_coordinate[ii],y_coordinate[ii]]
        D=np.array(D)
        epsilon=0.01
        minNeighbors=1
        dbscanner = Dbscan(D, minNeighbors, epsilon)
        dbscanner.run()
        datennoise=dbscanner.getNoise()
        datennoise=np.array(datennoise)
        b=[]
        cluster1=dbscanner.getClusterList()
        np.testing.assert_array_equal(cluster1,b)
        np.testing.assert_array_equal(datennoise,D)

    #@unittest.skip("2d_1cluster")
    def test_2d_1cluster(self):
        #create simple data:
        a = range(0, 11)
        print a
        d = []
        for i in a:
            for j in a:
                d.append([i*1.0, j*1.0])

        dbscanner = Dbscan(np.array(d), 3, 2.0)
        dbscanner.run()
        plotting.plotting(dbscanner.getClusterList(),dbscanner.getNoise())


    #@unittest.skip("test_plotting")
    def test_plotting(self):
        #some dummy data
        number, x_coordinate, y_coordinate = loadtxt('testdata/eps3-minpts5-cluster5-noise20.dat', unpack = True)
        D=[None]*len(x_coordinate)
        for ii in range(len(x_coordinate)):
            D[ii]=[x_coordinate[ii],y_coordinate[ii]]
        #put in the data we want to use
        minNeighbors = 5
        epsilon = 3.
        data = np.array(D, dtype=np.float64)
        # use dbscan
        dbscanner = Dbscan(data, minNeighbors, epsilon)
        dbscanner.run()
        # use plotting
        plotting.plotting(dbscanner.getClusterList(),dbscanner.getNoise())

    #@unittest.skip("test_neighbor_2d")
    def test_neighbor_2d(self):
        #create simple data:
        a = range(0, 11)
        d = []
        for i in a:
            for j in a:
                d.append(np.array([i*1.0, j*1.01]))
        n = Neighbor(np.array(d), 1.01)
        neighbors = n.nnPca(np.array([5.0,5.0]))
        print "neighbors of [5, 5] with eps = 1.01: " + str(neighbors)
        for n in neighbors:
            print d[n]
        assert len(neighbors) == 5


    #@unittest.skip("test_neighbor_3d")
    def test_neighbor_3d(self):
        #create simple data:
        a = range(0, 11)
        d = []
        for i in a:
            for j in a:
                for k in a:
                    d.append([i*1.0, j*1.0, k*1.0])
        n = Neighbor(np.array(d), 1.01)
        neighbors = n.nnPca(np.array([5*1.0,5*1.0,5*1.0]))
        print "neighbors of [5, 5, 5] with eps = 1.01: " + str(neighbors)
        assert len(neighbors) == 7

    #@unittest.skip("test_estimate_eps")
    def test_estimate_eps(self):
        D = np.linspace(0, 10, 21)
        print D
        test_average_nearest_distance=nearestneighbour.estimate_eps(D)
        print test_average_nearest_distance
        assert test_average_nearest_distance == 0.5

    #@unittest.skip("test_estimate_eps_10_dimension_data")
    def test_estimate_eps_10_dimension_data(self):
        D=np.zeros((7,2))
        for i in range(0, 7):
            for ii in range(0, 2):
                D[i,ii]=i+ii
        test_average_nearest_distance=nearestneighbour.estimate_eps(D)
        result=np.isclose(test_average_nearest_distance,np.sqrt(2))
        assert result
