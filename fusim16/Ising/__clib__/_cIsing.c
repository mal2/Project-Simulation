#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#define M_E 2.71828182845904523536028747135266249775724709369995

int randinit = 0;

void _randomInit(){
    srand(time(NULL));  
    randinit = 1;  
}

double _random(){
    /* random int between 0 and max-1 */
    double pf;
    if (randinit == 0)
        _randomInit();
    pf = (double) rand()/RAND_MAX;
    return pf;
        
}
int _randomInt(int max){
    if (randinit == 0)
        _randomInit();
    return rand()%max;
}

int _bc(int i, int m){
    if (i+1 > m)
        return 0;
    if (i < 0){
        return m-1;
    } else {
        return i;
    }
}

double _fastPow(double a, double b) {
	union {
		double d;
		int x[2];
	} u = { a };
	u.x[1] = (int)(b * (u.x[1] - 1072632447) + 1072632447);
	u.x[0] = 0;
	return u.d;
}

int _getEnergy(int* config, int m){
    int x, y, energy;
    energy = 0;
    for (x=0; x<m; x++){
        for (y=0; y<m; y++){
            energy -= _getLocalEnergy(config, x, y, m);
        }
    }
    return energy/2;
}

int _getLocalEnergy(int* config, int x, int y, int m) {
    return config[x*m+y]*(config[_bc(x+1,m)*m+y]+config[_bc(x-1,m)*m+y]+config[x*m+_bc(y+1,m)]+config[x*m+_bc(y-1,m)]);
}

int _getMag(int* config, int m){
    int x, y, mag;
    mag = 0;
    for (x=0; x<m; x++){
        for (y=0; y<m; y++){
            mag += config[x*m+y];
        }
    }
    return mag;
}

void _measure(int* config, int m, double* E1, double* E2, double* M1, double* M2){
    int Ene, Mag;
    Ene = _getEnergy(config, m);
    Mag = _getMag(config, m);
    
    E1[0] += Ene;
    E2[0] += (Ene * Ene);    
    M1[0] += Mag;
    M2[0] += (Mag * Mag);
}

void _oneMetropolisStep(int* config, double* delta, double beta, int m, int acstep){
    int x        = _randomInt(m);
    int y        = _randomInt(m);
    double pflip = _random();
    int dE       = 2*_getLocalEnergy(config, x, y, m);
    if (dE <= 0 || pflip < _fastPow(M_E,-beta*dE)){
//    if (dE < 0 || pflip < exp(-beta*dE)){
        config[x*m+y] *= -1;
        delta[acstep*2] = x;
        delta[acstep*2+1] = y;
    }
}

void _oneMetropolisStepWithMeasurement(int* config, double* delta, double beta, int m, int acstep, double* E1,double* E2,double* M1,double* M2){
    int i;    
    for (i=0; i<(m*m); i++){    
        _oneMetropolisStep(config, delta, beta, m, acstep);
    }
    _measure(config, m, E1, E2, M1, M2);
}

void _runMetropolis(int* config, double* delta, int m, double beta, int steps){
    int i;  
    for (i = 0; i < steps; i++){
        _oneMetropolisStep(config, delta, beta, m, i);
    }
}