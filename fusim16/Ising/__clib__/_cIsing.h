void   _randomInit();
double _random(int max);
int    _randomInt();
int    _bc(int i, int m);
double _fastPow(double a, double b);
int    _getEnergy(int* config, int m);
int    _getMag(int* config, int m);
void   _measure(int* config, int m, double* E1,double* E2,double* M1,double* M2);
int    _getLocalEnergy(int* config, int x, int y, int m);
void   _oneMetropolisStep(int* config, double* delta, double beta, int m, int acstep);
void   _oneMetropolisStepWithMeasurement(int* config, double* delta, double beta, int m, int acstep, double* E1, double* E2, double* M1, double* M2);
void   _runMetropolis(int* config, double* delta, int m, double beta, int steps);