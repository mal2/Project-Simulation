#!python
#cython: boundscheck=False
import numpy as __np__
cimport numpy as __np__

cdef extern from "_cIsing.h":  
    int _getEnergy(int* config, int m);    
    
cdef extern from "_cIsing.h":    
    void _oneMetropolisStep(int* config, double* delta, double beta, int m, int acstep);

cdef extern from "_cIsing.h":    
    void _oneMetropolisStepWithMeasurement(int* config, double* delta, double beta, int m, int acstep, double* E1, double* E2, double* M1, double* M2);

cdef extern from "_cIsing.h":
    void _runMetropolis(int* config, double* delta, int m, double beta, int steps);

def getEnergy(__np__.ndarray[int, ndim=2, mode='c'] config):
    return _getEnergy(<int*> __np__.PyArray_DATA(config), config.shape[0]);
    
def oneMetropolisStep(__np__.ndarray[int, ndim=2, mode='c'] config, __np__.ndarray[double, ndim=2, mode='c'] delta, double beta, int acstep):
    return _oneMetropolisStep(<int*> __np__.PyArray_DATA(config), <double*> __np__.PyArray_DATA(delta), beta, config.shape[0], acstep);

def oneMetropolisStepWithMeasurement(__np__.ndarray[int, ndim=2, mode='c'] config, __np__.ndarray[double, ndim=2, mode='c'] delta, double beta, int m, int acstep, __np__.ndarray[__np__.float64_t, ndim=1, mode='c'] E1, __np__.ndarray[__np__.float64_t , ndim=1, mode='c'] E2, __np__.ndarray[__np__.float64_t, ndim=1, mode='c'] M1, __np__.ndarray[__np__.float64_t, ndim=1, mode='c'] M2):
    return _oneMetropolisStepWithMeasurement(<int*> __np__.PyArray_DATA(config), <double*> __np__.PyArray_DATA(delta), beta, config.shape[0], acstep, <double*> __np__.PyArray_DATA(E1), <double*> __np__.PyArray_DATA(E2), <double*> __np__.PyArray_DATA(M1), <double*> __np__.PyArray_DATA(M2));

def runMetropolis(__np__.ndarray[int, ndim=2, mode='c'] config, __np__.ndarray[double, ndim=2, mode='c'] delta, int m, double beta, int steps):
    return _runMetropolis(<int*> __np__.PyArray_DATA(config), <double*> __np__.PyArray_DATA(delta), config.shape[0], beta, steps)
