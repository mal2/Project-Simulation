try:
    from setuptools import setup
    from setuptools import Extension
except ImportError:
    from distutils.core import setup
    from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy

modules=[
    Extension('cSystem', sources = ['cSytem.pyx', '_cIsing.c'], include_dirs=[numpy.get_include()])
    ]

setup(
  name = 'cSystem',
  ext_modules=modules,
  cmdclass = {'build_ext': build_ext}
)
