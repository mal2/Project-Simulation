# -*- coding: utf-8 -*-
import matplotlib.pyplot as __plt__
import matplotlib.animation as __animation__
import copy as __copy__
import time as __time__
import os as __os__
import numpy as __np__

class Figure:
    def __init__(self, initialConfig, delta, flipcount, imageList):
        self.initialConfig = initialConfig
        self.delta         = delta
        self.imageList     = imageList
        self.fig           = __plt__.figure()
        self.flipcount     = flipcount
        self.animationLength = 30000
        
        if self.flipcount == -1:
            if len(self.imageList) == self.imageList.shape[0]:
                self.imageList = __np__.reshape(self.imageList, (-1, self.imageList.shape[1],self.imageList.shape[1]))
            self.f = int(len(self.imageList)/(self.animationLength/1000*24))
            self.imageList = self.imageList[0::self.f]
        
    
    def makeImageList(self):
        print "Make ImageList ..."
        print self.flipcount
        self.imageList.append(self.initialConfig)
        self.tmp = __copy__.deepcopy(self.initialConfig)
        for i, site in enumerate(self.delta):
            self.tmp[site[0]][site[1]] *= -1
            if i in self.flipcount:
                tmp = __copy__.deepcopy(self.tmp)
                self.imageList.append(tmp)
        return
    
    def makeImageListMetropolis(self):
        self.imageList.append(self.initialConfig)
        self.tmp = __copy__.deepcopy(self.initialConfig)
        # partition delta such that we only calculate a frame for every 50ms
        self.flipsPerFrame = 1+int(20*len(self.delta)/self.animationLength)
        for i, site in enumerate(self.delta):
            if(site[0] != -1): self.tmp[site[0]][site[1]] *= -1
            if (i+1) % self.flipsPerFrame == 0 or i+1 == len(self.delta):
                tmp = __copy__.deepcopy(self.tmp)
                self.imageList.append(tmp)
        return
        
    def init(self):
        self.im = __plt__.imshow(self.imageList[0], interpolation="nearest")
        self.tit = __plt__.title('Step ', fontsize=24)
        return self.im,
        
    def updatefig(self, j):
        self.im.set_array(self.imageList[j])
        if self.flipcount != -1:
            if(len(self.flipcount) > 0): 
                self.tit.set_text('Step '+str(j+1) +' of '+str(len(self.flipcount)))
            else: 
                self.tit.set_text('Step ~' +str((j+1)*self.flipsPerFrame) +' of '+str(len(self.delta)))
        else:
            self.tit.set_text('Step '+str(int(j*self.f+1)))
        __plt__.draw()
        return self.im,
    
    def animate(self):
        if self.flipcount != -1:
            if(len(self.flipcount) > 0): 
                self.makeImageList()
            else: 
                self.makeImageListMetropolis()
        self.ani = __animation__.FuncAnimation(self.fig, self.updatefig, init_func=self.init, frames=len(self.imageList) , interval=(self.animationLength/1000)/len(self.imageList), blit=True, repeat_delay=5000)
        return self.ani
        
class Show:
    def __init__(self, *args, **kwargs):
        if len(args) == 3:
            self.figure = Figure(args[0], args[1], args[2], [])
        elif len(kwargs) == 1:
            self.figure = Figure([], [], -1, kwargs['config'])
        self.ani = self.figure.animate()
    
    def showPlot(self):
        """ Visualisiert die Simulation

        Note
        ----
        Entweder aufzurufen mit den Argumenten Start-Konfiguration, Delta-Array und FlipCount oder mit dem Keyword config.

        Parameters
        ----------
        Start-Konfiguration : array      
            Der Zustand des Systems kann zu jedem Zeitschritt nachverfolgt werden.
        Delta-Array : array      
            Das Delta-Array enthält Tupel (x,y). Jeder Eintrag ist ein Flip, der im Algorithmus durchgeführt wurde.
        FlipCount : array       
            Das Cluster-Größen-Array enthält Integer-Werte, die anzeigen, wieviele Felder das entsprechende Cluster enthält.
        config : [Optional] array
            Wenn gesetzt, wird das Array visualisiert. Z.B. bei arrays aus fromFile().
            
        Example
        -------
        >>> show = Ising.Plot.Show(initialConfig, delta, [])
        >>> show.showPLot()
        
        >>> show = Ising.Plot.Show(config=Config)
        >>> show.showPLot()
        """
        __plt__.show()
        #self.display_animation(self.ani)

    def saveVideo(self):
        """ Speichert die Simulation als animation*.mp4        
        """
        if (__os__.name == 'posix'):
            __plt__.rcParams['animation.ffmpeg_path'] ='/usr/bin/ffmpeg'
        else:
            __plt__.rcParams['animation.ffmpeg_path'] ='C:/Users/xandir/Downloads/ffmpeg-20160201-git-b62825a-win32-shared/bin/ffmpeg'
        fps=24#len(self.figure.imageList)/(self.figure.animationLength/1000)
        FFwriter = __animation__.FFMpegWriter(fps=fps,bitrate=2600)
        self.ani.save('animation-'+str(int(__time__.time()))+'.mp4', writer=FFwriter, fps=fps, extra_args=['-vcodec', 'libx264'])
        
class Graph:
    def printGraphs(self, T, Energy, Magnetization, SpecificHeat, Susceptibility):
        f = __plt__.figure(figsize=(18, 10), dpi=80, facecolor='w', edgecolor='k');    
#        __plt__.suptitle('Lattice Size = '+str(self.size)+', Steps = '+str(self.flipcount )+', Points = '+str(self.precission), fontsize=24)
        __plt__.suptitle('2D Ising Model Simulation', fontsize=24)
        
        f.add_subplot(2, 2, 1 );
        __plt__.plot(T, Energy, 'o', color="#A60628", label=' Energy');
        __plt__.xlabel("Temperature (T)", fontsize=20);
        __plt__.ylabel("Energy ", fontsize=20);
        
        f.add_subplot(2, 2, 2 );
        __plt__.plot(T, abs(Magnetization), '*', label='Magnetization');
        __plt__.xlabel("Temperature (T)", fontsize=20);
        __plt__.ylabel("Magnetization ", fontsize=20);
        
        
        f.add_subplot(2, 2, 3 );
        __plt__.plot(T, SpecificHeat, 'd', color="black", label='Specific Heat');
        __plt__.xlabel("Temperature (T)", fontsize=20);
        __plt__.ylabel("Specific Heat ", fontsize=20);
        
        
        f.add_subplot(2, 2, 4 );
        __plt__.plot(T, Susceptibility, '+', color="green", label='Specific Heat');
        __plt__.xlabel("Temperature (T)", fontsize=20);
        __plt__.ylabel("Susceptibility", fontsize=20);
        
        __plt__.draw()
        #__plt__.savefig('output/measure-'+str(int(time.time()))+'-'+str(N)+'-'+str(self.mcSteps)+'.png')
        __plt__.show()