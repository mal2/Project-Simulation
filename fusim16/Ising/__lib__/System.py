﻿# -*- coding: utf-8 -*-
from __future__ import division as __division__
import numpy as __np__
import sys as __sys__
import copy as __copy__
import time as __time__
try:
    from __clib__ import cSystem as __cSystem__
except ImportError:
    import cSystem as __cSystem__

class Init:
    def __init__(self, size=16, temperature=1):
        """ Initialisiert die Variablen
        
        Parameters
        ----------      
        size : int
            Die Seitenlänge, die das Gitter haben soll.
        temperature : float
            Die relative Temperatur in :math:`T [J/k_B]`
        """
        if(temperature <= 0 or temperature > 100): raise ValueError('Inkorrekte Temperatur')
        if(size < 1): raise ValueError('Inkorrekte Gitterlänge')
        self.J             = 1.
        self.size          = int(size)
        self.temperature   = float(temperature)
        self.beta          = 1./(self.temperature)
        self.config        = self.makeConfig()
        self.initialConfig = __copy__.deepcopy(self.config)
        __sys__.setrecursionlimit(self.size*self.size)

    def makeConfig(self):
        """ Erstellt ein Gitter mit zufälligen Spins :math:`s\in\{-1,1\}` 

        Returns
        -------
        int
            Array    
        """

        self.initial_config = __np__.random.random_integers(0,1,(self.size,self.size))*2 - 1
        self.initial_config = self.initial_config.astype(__np__.int32)
        return self.initial_config
        
    def randXY(self):
        """ Erzeugt zwei zufällige Zahlen :math:`x,y\in\{0,1,\dots,size-1\}`
        
        Returns
        -------
        int
            x, y

        """
        return __np__.random.randint(0, self.size), __np__.random.randint(0, self.size)

    def bc(self, i, size):
        """Prüft ob der Wert über die Gittergrenzen hinausgeht.
        
        Parameters
        ----------
        i : int
        size : int
            
        Returns
        -------
        int
            i wenn 0 <= i <= size
            
            0 wenn i+1 > size
            
            size-1 wenn i < 0
        """
        if i+1 > size:
            return 0
        if i < 0:
            return size-1
        else:
            return i

    def getEnergy(self, config):
        """ Berechnet die Energie des gesamten Gitters.
        
        Parameters
        ----------
        config : 
            Das Gitter
            
        Returns
        -------
        int
            Energie
        
        """
        energy = __cSystem__.getEnergy(config)
        return  energy

    def getMag(self, config):
        """Berechnet die Magnetisierung eines Gitters.
        
        Parameters
        ----------
        config :
            Die aktuelle Konfiguration
            
        Returns
        -------
        int
            Magnetisierung
        """
        return __np__.sum(config)

    def getJ(self):
        return self.J

    def getSize(self):
        return self.size

    def getTemp(self):
        return self.temperature

    def getBeta(self):
        return self.beta
    
    def toFile(self, filename, config, delta):
        """Schreibt die Simulation als Array ((steps*size) Zeilen, size Spalten) mit numpy.memmap in die Datei 'filename_size_steps.npy'.
        
        Parameters
        ----------
        filename : 
            Name der Datei
            
        config :
            Die erste Konfiguration (Grundkonfiguraion)
            
        delta : 
            Die Liste der Änderungen der Spins, die auf dem Gitter ausgeführt werden.
                   
        """
        fp = __np__.memmap(str(filename)+'_'+str(config.shape[0])+'_'+str(len(delta)+1)+'.npy', dtype='int32', mode='w+', shape=(config.shape[0]*(len(delta)+1),config.shape[0]))        
        fp[0:config.shape[0]] = config
        for i, site in enumerate(delta):
            if site[0] != -1:
                config[site[0]][site[1]] *= -1
            fp[(i+1)*(config.shape[0]):(i+2)*(config.shape[0])] = config
        del fp
            
    def fromFile(self, filename):
        """Lädt die Datei 'filename' mit Hilfe von numpy.memap.
        
        Parameters
        ----------
        filename : 
            Name der Datei
            
        Returns
        -------
        memmap
            Array ((steps*size) Zeilen, size Spalten)
        
        """
        n = int(filename.split('_')[1])
        steps = int(filename.split('_')[2].split('.')[0])
        fpr = __np__.memmap(filename, dtype='int32', mode='c', shape=(n*steps,n))
        return fpr
        
    def toBitFile(self, filename, config, delta):
        """Schreibt die Simulation als Bitarray in die Datei 'filename'.
        
        Parameters
        ----------
        filename : 
            Name der Datei
            
        config :
            Die erste Konfiguration (Grundkonfiguraion)
            
        delta : 
            Die Liste der Änderungen der Spins, die auf dem Gitter ausgeführt werden
        
        """
        length = config.shape[0]
        config = (config+1)/2
        config = config.astype(__np__.uint8).flatten()
        with open(str(filename),'wb') as f_handle:
            __np__.packbits(config).tofile(f_handle)
            for site in delta:
                if site[0] != -1:
                    config[site[0]*length+site[1]] = 1-config[site[0]*length+site[1]]                
                    __np__.packbits(config).tofile(f_handle)
                else:
                    __np__.packbits(config).tofile(f_handle)
    
    def fromBitFile(self, filename):
        """Lädt die Bitarray-Datei 'filename' 
        
        Parameters
        ----------
        filename : 
            Name der Datei
            
        Returns
        -------
        int
            Array
        
        """
        system = []
        with open(str(filename),'rb') as f_handle:
            x = __np__.fromfile(f_handle, dtype=__np__.uint8)
        system = __np__.array(__np__.u__np__.ckbits(x), dtype=__np__.uint8)
        return system
        
class Wolff(Init):
    def __init__(self, size, temperature):
        """ Initialisiert Variablen

        Parameters
        ----------
        size : int
            Die Seitenlänge, die das Gitter haben soll.
        temperature : float
            Die relative Temperatur in :math:`\dfrac{T}{T_C}`

        Example
        -------

        >>> import ising
        >>> model = ising.lib.Wolff(200,1.3)
        """
        Init.__init__(self, size, temperature)
        self.delta         = []
        self.p             = float(1-__np__.exp(-2.*self.J*self.beta))
        self.counter       = 0
        self.flipCount     = []
        self.clusterSteps  = 0

    def oneClusterStep(self):
        """ Führt einen Cluster-Schritt des Wolff-Algorithmus' auf dem Gitter durch und überschreibt dieses dann mit der entstandenen Konfiguration. """
        self.clusterSteps += 1
        self.x, self.y = self.randXY()
        self.oldSpin = self.config[self.x][self.y]
        self.growCluster(self.x, self.y)
        self.flipCount.append(self.counter)

    def growCluster(self, x, y):
        """ Probiert das Cluster vom Giiterpunkt x,y aus zu erweitern. Im Wolff-Algorithmus wird in jedem Cluster-Schritt ein Cluster von einem zufällig gewählten Gitterpunkt gebildet. Dabei wird der Zustand des Gitterpunktes gespeichert und angrenzende Felder mit gleichem Spin werden iterativ mit der Wahrscheinlichkeit :math:`P(x,y) = 1 - e^{-2 J/T}` hinzugefügt. Anschließend wird das gesamte Cluster geflipt.  
        
        Parameters
        ----------
        x : int
            Zeile des Gitters
        y : int
            Spalte des Gitters  
        """
        self.counter += 1
        self.config[x,y] *= -1
        self.delta.append([x,y])

        xprev = self.bc(x-1, self.size)
        xnext = self.bc(x+1, self.size)
        yprev = self.bc(y-1, self.size)
        ynext = self.bc(y+1, self.size)

        for site in [[xprev,y],[xnext,y],[x,yprev],[x,ynext]]:
             if self.config[site[0]][site[1]] == self.oldSpin and __np__.random.rand() < self.p:
                self.growCluster(site[0], site[1])

    def run(self, steps):
        """Führt den Wolff-Algorithmus auf der gegebenen Konfiguration aus.
        
        Parameters
        ----------
        steps : int
            Anzahl der Cluster-Schritte, die ausgeführt werden sollen.

        Returns
        -------        
        Start-Konfiguration : array      
            Der Zustand des Systems kann zu jedem Zeitschritt nachverfolgt werden          
        Delta-Array : array      
            Das Delta-Array enthält Tupel (x,y). Jeder Eintrag ist ein Flip, der im Algorithmus durchgeführt wurde.          
        FlipCount : array       
            Das Cluster-Größen-Array enthält Integer-Werte, die anzeigen, wieviele Felder das entsprechende Cluster enthält.
            
        Der Zustand des Systems nach dem ersten Cluster-Schritt kann also berechnet werden, indem die ersten k Tupel des Delta-Arrays in der Start-Konfiguration geflippt werden, wobei k der erste Eintrag des Cluster-Größen-Arrays ist.
        """
        while (steps == 0 and abs(self.getMag(self.config)) < (self.size*self.size*self.p)) or self.counter < steps:
            self.oneClusterStep()
        return self.initialConfig, self.delta, self.flipCount

#class Metropolis(Init): #pure python implementation
#    def __init__(self, size, temperature):
#        """ Initialisiert Variablen
#
#        Parameters
#        ----------
#        size : int
#            Die Seitenlänge, die das Gitter haben soll.
#        temperature : float
#            Die relative Temperatur in :math:`\dfrac{T}{T_C}`
#
#        Example
#        -------
#        >>> import ising
#        >>> model = ising.lib.Metropolis(200,1.3)
#        """
#        Init.__init__(self, size, temperature)
#        self.flips = 0
#        self.delta = []
#
#    def oneStep(self):
#        """Führt einen Schritt des Metropolis-Algorithmus auf der gegebenen Konfiguration aus, und überschreibt diese dann."""
#        x, y = self.randXY()
#        Delta = 2*self.getLocalEnergy(self.config, x, y)
#        if(Delta < 0 or __np__.random.rand() < __np__.exp(-self.beta*Delta)):
#            self.config[x][y] *= -1
#            self.delta.append([x,y])
#            self.flips += 1
#        else:
#            self.delta.append([-1,-1])
#    
#    def oneMeasureStep(self, i, E1, E2, M1, M2):    
#        self.oneStep()
#        Ene = self.getEnergy(self.config)        # calculate the energy
#        Mag = self.getMag(self.config)           # calculate the magnetisation
#
#        E1 = E1 + Ene
#        M1 = M1 + Mag
#        M2 = M2 + Mag*Mag
#        E2 = E2 + Ene*Ene
#        return E1, E2, M1, M2
#
#    def run(self, steps):
#        """Führt den Metropolis-Algorithmus auf der gegebenen Konfiguration aus.
#        
#        Parameters
#        ----------
#        steps : int
#            Anzahl der Schritte, die ausgeführt werden sollen.
#        
#        Returns
#        -------
#        Start-Konfiguration : array  
#            Der Zustand des Systems kann zu jedem Zeitschritt nachverfolgt werden           
#        Delta-Array : array       
#            Das Delta-Array enthält Tupel (x,y). Jeder Eintrag ist ein Flip, der im Algorithmus durchgeführt wurde.
#        """
#        starttime = __time__.time()
#        while len(self.delta) < steps:
#            self.oneStep()
##        print "Finished calculation in ", len(self.delta),"steps doing",self.flips,"flips in", __time__.time()-starttime,"s."
#        return self.initialConfig, self.delta

class Metropolis(Init):
    def __init__(self, size, temperature):
        """ Initialisiert Variablen

        Parameters
        ----------
        size : int
            Die Seitenlänge, die das Gitter haben soll.
        temperature : float
            Die relative Temperatur in :math:`\dfrac{T}{T_C}`
            
        Example
        -------
        >>> import ising
        >>> model = ising.lib.Metropolis(200,1.3)
        """
        Init.__init__(self, size, temperature)

    def oneStep(self, i):
        """Führt einen Schritt des Metropolis-Algorithmus auf der gegebenen Konfiguration aus, und überschreibt diese dann."""
        __cSystem__.oneMetropolisStep(self.config, self.delta, self.beta, i)
        return self.config

    def oneMeasureStep(self, acstep, E1, E2, M1, M2):
        """Führt einen Schritt des Metropolis-Algorithmus auf der gegebenen Konfiguration aus, und misst verschiedene Werte.
        
        Parameters
        ----------
        acstep : int
            Aktueller Schritt innnerhalb der Schleife (z.B das i in "for in range(steps)")
        E1 : int
        E2 : int
        M1 : int
        M2 : int
        """
        __cSystem__.oneMetropolisStepWithMeasurement(self.config, self.delta, self.beta, self.size, acstep, E1, E2, M1, M2)

    def run(self, steps):
        """Führt den Metropolis-Algorithmus auf der gegebenen Konfiguration mit C-Code aus.
        
        Parameters
        ----------
        steps : int
            Anzahl der Schritte, die ausgeführt werden sollen.

        Returns
        -------        
        Start-Konfiguration : array
        
            Der Zustand des Systems kann zu jedem Zeitschritt nachverfolgt werden
            
        Delta-Array : array
        
            Das Delta-Array enthält Tupel (x,y). Jeder Eintrag ist ein Flip, der im Algorithmus durchgeführt wurde.
        """
        self.delta = __np__.zeros((steps,2))-1
        __cSystem__.runMetropolis(self.config, self.delta, self.size, self.beta, steps)
        return self.initialConfig, self.delta

    def runPartial(self, steps):
        """Führt den Metropolis-Algorithmus auf der gegebenen Konfiguration mit partiellem C-Code aus.
        
        Parameters
        ----------
        steps : int
            Anzahl der Schritte, die ausgeführt werden sollen.

        Returns
        -------        
        Start-Konfiguration : array
        
            Der Zustand des Systems kann zu jedem Zeitschritt nachverfolgt werden
            
        Delta-Array : array
        
            Das Delta-Array enthält Tupel (x,y). Jeder Eintrag ist ein Flip, der im Algorithmus durchgeführt wurde.
        """
        self.delta = __np__.zeros((steps,2))-1
        i = 0
        while i < steps:
            __cSystem__.oneMetropolisStep(self.config, self.delta, self.beta, self.size, i)
            i += 1
        return self.initialConfig, self.delta

class Measure(Init):
    def __init__(self, size=16, precission=128, flipcount = 8000):
        self.size = size
        self.temperature = 1
        self.precission = precission
        self.flipcount = flipcount
        Init.__init__(self, self.size, self.temperature)
        
    def measureSystem(self):
        """Führt den Metropolis-Algorithmus C-Code für Temperaturen von 1 bis 3 aus und nimmt Messungen der Energie, Magnetisierung, Spezifischen Wärme und der Suszeptibilität vor.
        
        Parameters
        ----------
        size : int
        
            Größe des Gitters

        precission : int

            Anzahl der Messpunkte

        flipcount : int

            Anzahl der Schritte pro Temperatur

        Returns
        -------        
        Temperatur : array
            
        Energie : array
            
        Magnetisierung : array
        
        Spezifischen Wärme : array
        
        Suszeptibilität : array
        
        """
        
        Energy         = __np__.zeros(self.precission)
        Magnetization  = __np__.zeros(self.precission)
        SpecificHeat   = __np__.zeros(self.precission)
        Susceptibility = __np__.zeros(self.precission)
        
        T1  = __np__.linspace(1, 2, num=int(self.precission/4), endpoint=False)
        T2  = __np__.linspace(2, 2.5, num=int(self.precission/2), endpoint=False)
        T3  = __np__.linspace(2.5, 4, num=int(self.precission/4))
        T   = __np__.append(__np__.append(T1,T2),T3) #temperature

        for m in range(len(T)):
            perc = m/len(T)*100
            __sys__.stdout.write("Measuring:"+"%3d%%\r" % perc)
            __sys__.stdout.flush()

            mcSteps = int(self.flipcount/2)
            N       = self.size
            
            syst = Metropolis(self.size,T[m])
            self.initialConfig, self.delta = syst.run(mcSteps) #Equilibriate
           
            E1 = __np__.zeros(1)
            E2 = __np__.zeros(1)
            M1 = __np__.zeros(1)
            M2 = __np__.zeros(1)
            
            for i in range(mcSteps):
                syst.oneMeasureStep(i, E1, E2, M1, M2) # with C Metropolis                
                
            Energy[m]         = E1[0] / (mcSteps*N*N)
            Magnetization[m]  = M1[0] / (mcSteps*N*N)
            SpecificHeat[m]   = (E2[0]/mcSteps - E1[0]*E1[0]/(mcSteps*mcSteps))/(N*T[m]*T[m]);
            Susceptibility[m] = (M2[0]/mcSteps - M1[0]*M1[0]/(mcSteps*mcSteps))/(N*T[m]*T[m]);

        return T, Energy, Magnetization, SpecificHeat, Susceptibility