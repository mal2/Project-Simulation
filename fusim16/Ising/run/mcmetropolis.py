#!/usr/bin/python
# -*- coding: utf-8 -*-
from fusim16.Ising import System as lib
from fusim16.Ising import Plot as plot

steps = 100000

N = 32
T = 1

def systemrun(showvideo=0):
    #in python
    sys = lib.Metropolis(N,T)
    initialConfig, delta = sys.run(steps)
    
    if showvideo == 1:
        show = plot.Show(initialConfig, delta, [])
        #show.saveVideo()    
        show.showPlot()
    
#def csystemrun(showvideo=0, fname=0):
#    #ganzer loop in c
#    sys = lib.cMetropolis(N,T)
#    initialConfig, delta = sys.run(steps)
#    print delta
#    print initialConfig
#
#    if fname != 0: 
#        print("write to "+str(fname))
#        sys.toFile(fname,initialConfig,delta)
##        system = sys.fromFile('output\out.bin') 
#
#    if showvideo == 1:
#        show = plot.show(initialConfig, delta, [])
#        #show.saveVideo()    
#        show.showPlot()    
#def csystemrunPartial(showvideo=0):
#    #nur onestep in c implementiert
#    sys = lib.cMetropolis(N,T)
#    initialConfig, delta = sys.run(steps) 
#    
#    if showvideo == 1:
#        show = plot.show(initialConfig, delta, [])
#        #show.saveVideo()    
#        show.showPlot()
         
#print "Run whole loop in C"
systemrun(showvideo=1)
#csystemrun(fname='output/out_'+str(N)+'_'+str(steps)+'.bin')