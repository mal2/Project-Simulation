#!/usr/bin/python
# -*- coding: utf-8 -*-
from fusim16.Ising import System as lib
from fusim16.Ising import Plot as plot
import sys

if len(sys.argv) != 3:
    N = 128
    T = 2.26
else:
    N = sys.argv[1]
    T = sys.argv[2]

def systemrun():
    sys = lib.Wolff(N, T)
    initialConfig, delta, flipcount = sys.run(100000)
   
    show = plot.Show(initialConfig, delta, flipcount)
    show.showPlot()
    
systemrun()