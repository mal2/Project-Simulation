#!/usr/bin/python
from fusim16.Ising import System as lib
from fusim16.Ising import Plot as plot

init = lib.Measure(8,64,10000)
plot = plot.Graph()
T, Energy, Magnetization, SpecificHeat, Susceptibility = init.measureSystem()
plot.printGraphs(T, Energy, Magnetization, SpecificHeat, Susceptibility)