from __future__ import division
import unittest
import numpy as np
from fusim16.Ising import System as lib

class TestIsingMethods(unittest.TestCase):

  def test_init(self):
      # lambda verhindert, dass die exception geworfen wird
      
      self.assertRaises(ValueError, lambda: lib.Wolff(-3,2.26))
      self.assertRaises(ValueError, lambda: lib.Wolff(5,0))
      self.assertRaises(ValueError, lambda: lib.Wolff(5,200))
      
  def test_get_functions(self):
      wolff=lib.Wolff(32,50)
      metropolis=lib.Metropolis(32,50)
      
      self.assertTrue(wolff.getBeta()==1.0/50)
      self.assertTrue(wolff.getTemp()==50)
      self.assertTrue(wolff.getSize()==32)
      
      self.assertTrue(metropolis.getBeta()==1.0/50)
      self.assertTrue(metropolis.getTemp()==50)
      self.assertTrue(metropolis.getSize()==32)
      
  def test_energy_function(self):
      wolff=lib.Wolff(32,50)
      metropolis=lib.Metropolis(32,50)
      
      self.assertRaises(ValueError, lambda: wolff.getEnergy(np.zeros((4)).astype(np.int32)))
      self.assertTrue(wolff.getEnergy(np.ones((32,32)).astype(np.int32))==-1*2*32*32*wolff.getJ())
      self.assertTrue(wolff.getEnergy(np.zeros((32,32)).astype(np.int32)-1)==-1*2*32*32*wolff.getJ())
      
      self.assertRaises(ValueError, lambda: metropolis.getEnergy(np.zeros((4)).astype(np.int32)))
      self.assertTrue(metropolis.getEnergy(np.ones((32,32)).astype(np.int32))==-1*2*32*32*metropolis.getJ())
      self.assertTrue(metropolis.getEnergy(np.zeros((32,32)).astype(np.int32)-1)==-1*2*32*32*metropolis.getJ())
      
      self.assertTrue(metropolis.getEnergy(np.array([[1,1,1],[1,-1,1],[1,1,1]]))==-2*(9-4)*metropolis.getJ())
      self.assertTrue(metropolis.getEnergy(np.array([[-1,1,1],[1,1,1],[1,1,1]]))==-2*(9-4)*metropolis.getJ())
      self.assertTrue(metropolis.getEnergy(np.array([[-1,1,-1],[1,-1,-1],[1,-1,1]]))==6*metropolis.getJ())

  def test_distribution(self):
      cmeasure = lib.Measure(8,128,8000)
      T, Energy, Magnetization, SpecificHeat, Susceptibility = cmeasure.measureSystem()
     
      # Checks if the slope of the Energy-Temperature-Diagram between 2.2 < T < 2.4 is correct
      data = zip(T,Energy)
      data = [x for x in data if x[0]>2.2]      
      data = [x for x in data if x[0]<2.4]
      xE = zip(*data)[0]
      yE = zip(*data)[1]
      zE = np.polyfit(xE, yE, 1)

      self.assertTrue(-zE[1]/2.269185 >= 0.88)
      
      # Checks if the Points of the Magnetization-Temperature-Diagram for T < 1.9 are in Range of 4% of (1-np.sinh(2/x)**(-4))**(1/8)
      data = zip(T,Magnetization)
      data = [x for x in data if x[0]<1.9] 
      xM = zip(*data)[0]
      yM = np.array(zip(*data)[1])
      zM = np.array([(1-np.sinh(2/x)**(-4))**(1/8) for x in xM])
      dM = abs(yM/zM)
      
      [self.assertTrue(False) for x in dM if 1.04 < x < 0.96]
      
      # Checks right slope of Specific Heat
      data = zip(T,Energy)
      data = [x for x in data if x[0]>2.3]      
      data = [x for x in data if x[0]<3]
      xCr = zip(*data)[0]
      yCr = np.array(zip(*data)[1])
      zCr = np.array([(80.28/x**4) for x in xCr])
      dMr = yCr/zCr

      [self.assertTrue(False) for x in dMr if 1.12 < x < 0.88]
      
      # Checks left slope of Specific Heat
      data = zip(T,Energy)
      data = [x for x in data if x[0]>2]
      xCl = zip(*data)[0]
      yCl = np.array(zip(*data)[1])
      zCl = np.array([(0.0947*x**4) for x in xCl])
      dMl = yCl/zCl

      [self.assertTrue(False) for x in dMl if 1.12 < x < 0.88]
      
      # Checks right slope of Susceptibility
      data = zip(T,Energy)
      data = [x for x in data if x[0]>2.3]      
      data = [x for x in data if x[0]<3]
      xCr = zip(*data)[0]
      yCr = np.array(zip(*data)[1])
      zCr = np.array([(7576/x**6) for x in xCr])
      dMr = yCr/zCr

      [self.assertTrue(False) for x in dMr if 1.12 < x < 0.88]
      
#      np.savetxt('output/Temp.out', T)   # X is an array
#      np.savetxt('output/Energy.out', Energy)
#      np.savetxt('output/Magnetization.out', Magnetization)
#      np.savetxt('output/SpecificHeat.out', SpecificHeat)
#      np.savetxt('output/Susceptibility.out', Susceptibility)
      
  
if __name__ == '__main__':
    unittest.main()