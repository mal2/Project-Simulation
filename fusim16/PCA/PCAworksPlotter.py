﻿import numpy as np
import pca
import matplotlib.pyplot as plt

# 0:size - Class1, size:end - Class2
size = 100
numberOfTestUnits = 10

for i in range (numberOfTestUnits):
    # for gaussian plot
    plotTogether = False
    plotSeperate = True

    #for kernel plot
    showKernelPlot = True



    # gaussian distribution

    x = np.load("unitTest/testData/" + "testGaussianClasses" + str(i+1) + ".npy")
        


    # do PCA
    p = pca.pca(x, 1, mode="svd")

    z1 = np.zeros( len(p[0,:size]) )
    z2 = np.zeros( len(p[0,size:]) )        


    if plotTogether:
        fig, ax = plt.subplots(2, 1)
        ax[0].plot (x[0,:100], x[1,:size], "o", linewidth=2, label = "distribution 1")
        ax[0].plot (x[0,100:], x[1,size:], "o", linewidth=2, label = "distribution 2")
        ax[1].plot (p[0,:size], z1, "o", linewidth=2, label = "distribution 1")
        ax[1].plot (p[0,size:], z2, "o", linewidth=2, label = "distribution 2")
        #plt.legend()
        plt.show()

    if plotSeperate:
        fig, ax = plt.subplots(2, 1)
        ax[0].set_title("before linear PCA")
        ax[0].plot (x[0,:100], x[1,:size], "o", linewidth=2, label = "distribution 1")
        ax[1].set_title("after linear PCA")
        ax[1].plot (z1, p[0,:size], "o", linewidth=2, label = "distribution 1")
        plt.show()

        fig, ax = plt.subplots(2, 1)
        ax[0].set_title("before linear PCA")
        ax[0].plot (x[0,100:], x[1,size:], "o", linewidth=2, label = "distribution 2")
        ax[1].set_title("after linear PCA")
        ax[1].plot (z2, p[0,size:], "o", linewidth=2, label = "distribution 2")
        plt.show()



    # circles
    x = np.load("unitTest/testData/" + "testCircles" + str(i+1) + ".npy")

    if showKernelPlot:
        fig, ax = plt.subplots(2, 1)
        ax[0].set_title("before kernel PCA")
        ax[0].plot (x[0,x[2,:] == 0], x[1,x[2,:] == 0], "o", linewidth=2, label = "distribution 1")
        ax[0].plot (x[0,x[2,:] == 1], x[1,x[2,:] == 1], "o", linewidth=2, label = "distribution 1")


    y = x[2,:]
    x = x[0:2,:]

    # do PCA
    p = pca.pca(x, 1, mode="kernel")


    if showKernelPlot: 
        p = np.reshape(p, (1, 1000))
        z = np.zeros(500)
        y = np.reshape(y, (1, 1000))
        p = np.concatenate((p, y), axis=0)

        ax[1].set_title("after kernel PCA")
        ax[1].plot (p[0,p[1,:] == 0], z, "o", linewidth=2, label = "distribution 1")
        ax[1].plot (p[0,p[1,:] == 1], z, "o", linewidth=2, label = "distribution 1")
        plt.show()