## Softwareproject WS 15/16: Computational Science / PCA

## Usage:

For data, which fits into RAM:

	import pca

	projectedData = pca.pca(X, k, mode="cov", sigma=0.5)

where

	X: 2D numpy.ndarray
		Data matrix of dimensions MxN with M features and N samples
	k: int
		k first dimensions with largest variances, onto which data should be projected
	mode: str
		Implementation method: 'cov', 'svd', 'kernel'
	sigma: float, optional, default: 0.5
		Standard deviation of Gaussian RBF kernel
	projectedData: 2D numpy.ndarray
		Projected data

For data, which exceeds RAM:

	import pca

	projectedData = pca.pca(X, k)

where

	X: 2D numpy.core.memmap
		See above.
	k: int
		See above.
	projectedData: 2D numpy.core.memmap
		Projected data

Note: The file underlying the memmap-object is of temporary nature and will be automatically removed on program termination.
