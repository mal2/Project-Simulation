﻿import fusim16.Ising as ising
import fusim16.PCA as pca
import numpy as np
import matplotlib.pyplot as plt



samples = 500
dimensionRoot = 16
temp = 1


system = ising.System.Metropolis(dimensionRoot, temp)

system.run(samples)
initalConfig, configChanges = system.run(samples)

system.toFile('main', initalConfig, configChanges)


obsList = system.fromFile("main_"+str(dimensionRoot)+'_'+str(samples+1)+".npy")

X = np.zeros((dimensionRoot * dimensionRoot, samples))
for idx in range(samples):
    X[:,idx] = obsList.reshape(-1, dimensionRoot * dimensionRoot)[idx]


wantedDim = 3

pX, eigVec = pca.pca(X, 3, "cov", p = 0.8, returnEigVec=True)

k = len(eigVec)
spinGrids = []

for i in range ( k ):
    spinGrids.append( eigVec[i].reshape( (dimensionRoot, dimensionRoot) ) )



# define the grid over which the function should be plotted (xx and yy are matrices)

x = np.arange(0, dimensionRoot + 1, 1.)
xx, yy = np.meshgrid(x,x)

for i in range( k ):
    # fill a matrix with the function values
    zz = spinGrids[i]

    # set plot title and axis names
    plt.xlabel( "x-Position" )
    plt.ylabel( "y-Position" )
    plt.title("Eigenvector " + str(i + 1))

    # plot the calculated function values
    plt.pcolor(xx, yy, zz, vmin = -1, vmax = 1)

    # and a color bar to show the correspondence between function value and color
    plt.colorbar()

    plt.savefig( "plt/eigenVector_" + str(i) )
    #plt.show() 

    plt.close()

