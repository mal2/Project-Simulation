import pca
import sklearn.datasets as dts
import numpy as np

# Generate gaussian distributions, where x[:,:size] from one class
# and x[:, size:] from another class. Scale size to your likings!
size = 1000
numberOfTestUnits = 10

for i in range (numberOfTestUnits):
    x = np.random.multivariate_normal([10, 10], [[1,0],[0,50]], size).T
    y = np.random.multivariate_normal([10, 60], [[1,0],[0,50]], size).T
    x = np.concatenate((x,y), axis=1)
    np.save("unitTest/testData/" + "testGaussianClasses" + str(i+1), x)

    # Memory mapped version
    xm = np.memmap("unitTest/testData/" + "testGaussianClassesMmap" + str(i+1) + ".npy", dtype="float64", mode="w+", shape=(2, size*2))
    xm[:] = x
    xm.flush()


    #do PCA
    x = np.load("unitTest/testData/" + "testGaussianClasses" + str(i+1) + ".npy")
    p = pca.pca(x, 1, mode="svd")

    # save data of pca
    np.save("unitTest/testData/" + "testGaussianClassesTransformed" + str(i+1), p)


    # Generate concentric circles
    x, y = dts.make_circles(n_samples=1000, noise=0.1, factor=0.25)
    y = np.reshape(y, (1000,1)) # These are class labels: 0, 1
    x = np.concatenate((x,y), axis=1)
    np.save("unitTest/testData/" + "testCircles" + str(i+1), x.T)

    # Memory mapped circles
    #xm = np.memmap("unitTest/testData/" + "testCirclesMmap" + str(i+1) + ".npy", dtype="float64", mode="w+", shape=(3, 1000))
    #xm[:] = x.T
    #xm.flush()


    #do PCA
    x = np.load("unitTest/testData/" + "testCircles" + str(i+1) + ".npy")
    p = pca.pca(x, 1, mode="kernel")

    # save data of pca
    np.save("unitTest/testData/" + "testCirclesTransformed" + str(i+1), p)





