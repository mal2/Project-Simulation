﻿import numpy as np
import scipy.spatial.distance as dst
import tempfile # For memory mapped class attributes

class PCA:
    """ Apply principal component on a given data set
        for the plain purpose of dimensionality reduction.

    Attributes
    ----------
    data: 2D numpy.ndarray
        Data matrix of dimensions MxN with M features and N samples.
        It must hold that N >= M.
    m: int
        Number of features of data
    n: int
        Number of samples of data
    k: int
        k-first features data should be projected onto. It must hold that 0 < k <= M.
    p: float, optional, default=0.
        Information preservation (0,1]. Ignored, if zero.

    """

    def __init__(self, X, k, p=0.):
        """ Constructor. """

        # Assertions
        assert type(X) is np.ndarray, "X is no numpy.ndarray"
        assert len(X.shape) == 2, "X mishaped"
        assert X.shape[0] <= X.shape[1], "Less observations than features"
        assert type(k) is int, "k is no integer"
        assert k > 0 and k <= X.shape[0], "Must hold that 0 < k <= M"
        assert type(p) is float, "p is no float"
        assert p >= 0. and p <= 1., "Must hold that 0 <= p <= 1"

        self.data = X
        self.k = k
        self.p = p
        self.m = self.data.shape[0]
        self.n = self.data.shape[1]


    def kPCA(self, sigma=0.5):
        """ Apply PCA on linearly inseparable data using a gaussian radial basis
            function kernel.

        Parameters
        ----------
        sigma:
            Standard deviation of Gaussian radial basis function

        Returns
        -------
        2D numpy.ndarray
            Data of dimensions kxN, which has been projected on first k
            dimensions with largest variances.

        """

        # Compute distances between data points and store them within a (quadratic) matrix
        m = dst.squareform(dst.pdist(self.data.T, "sqeuclidean"))

        # Build kernel
        kern = np.exp(-(1. / (2. * sigma * sigma)) * m)

        # Center kernel
        s = np.ones(kern.shape) / kern.shape[0]
        kern = kern - s.dot(kern) - kern.dot(s) + s.dot(kern).dot(s)

        # Ascending eigenvectors
        val, vec = np.linalg.eigh(kern)
        self.k = presQual(val[::-1], self.k, self.p)

        # Return projected data
        return np.fliplr(vec)[:,:self.k].T


    def project(self, transMat):
        """ Project data along k features with greatest variances.

        Parameters
        ----------
        transMat: numpy.ndarray
            Transformation matrix of dimensions MxM

        Returns
        -------
        2D numpy.ndarray
            Data of dimensions kxN, which has been projected on first k
            dimensions with largest variances.

        """

        # Project and return data
        return transMat[:self.k,:].dot(self.data)


    def cov(self):
        """ Apply PCA using covariance matrix and eigenvectors.

        Returns
        -------
        2D numpy.ndarray
            See project().

        """

        # Center data
        self.data -= np.mean(self.data, axis=1)[:, None]

        # Compute covariance matrix
        covMat = np.cov(self.data)

        # Determine eigenvalues and eigenvectors (of form MxM)
        eigVal, eigVec = np.linalg.eigh(covMat)

        # Look at most important features
        self.k = presQual(eigVal[::-1], self.k, self.p)

        # Sort eigenvectors by eigenvalues
        eigVec = eigVec[:, eigVal.argsort()[::-1]]
        self.transMat = eigVec[:self.k,:] # Debug purpose regarding eigenvectors

        # Return data, which has been projected on k-first eigenvectors
        return self.project(eigVec.T)


    def svd(self):
        """ Apply PCA using singular value decomposition.

        Returns
        -------
        2D numpy.ndarray
            See project().

        """

        # In case of SVD subtract mean off of data
        self.data -= np.mean(self.data, axis=1)[:, None]

        # Construct magic helper matrix according to paper
        Y = self.data.T / np.sqrt(self.n - 1)

        # Apply u,s,v = SVD, where v can be interpreted as transformation matrix;
        # Important: Numpy's v is the transposed matrix of Octave/Matlab's v
        eigVal, transMat = np.linalg.svd(Y)[1:]

        # Adjust self.k to preserve a certain quality, if requested
        self.k = presQual(eigVal**2, self.k, self.p)

        # Project data along first k features with largest variances
        return self.project(transMat)


class PCAMmap:
    """ Apply PCA for memory mapped data for the purpose of dimensionality
        reduction. It is assumed, that multiple square matrices of size MxM
        will fit into memory. For member descriptions, see PCA.

    """

    def __init__(self, X, k, p=0.):

        # Assertions
        assert type(X) is np.core.memmap, "X is no memmap-object"
        assert len(X.shape) == 2, "X mishaped"
        assert X.shape[0] <= X.shape[1], "Less observations than features"
        assert X.shape[1] % X.shape[0] == 0, "In case of memmap, M needs to divide N evenly"
        assert type(k) is int, "k is no integer"
        assert k > 0 and k <= X.shape[0], "Must hold that 0 < k <= M"
        assert type(p) is float, "p is no float"
        assert p >= 0. and p <= 1., "Must hold that 0 <= p <= 1"

        self.data = X
        self.m = X.shape[0]
        self.n = X.shape[1]
        self.k = k
        self.p = p


    def kPCA(self, sigma=0.5):
        """ Process data into kernel space, i.e. try to make linearly inseparable
            data linearly separable.

        Parameters
        ----------
        sigma:
            Standard deviation of Gaussian radial basis function
        """
        raise Exception("kPCA() is yet to be implemented")


    def project(self, transMat):
        """ Project data along k features with greatest variances.

        Parameters
        ----------
        transMat: numpy.ndarray
            Transformation matrix of dimensions MxM

        Returns
        -------
        2D numpy.core.memmap
            Data of dimensions kxM, which has been projected on first k
            dimensions with largest variances.

        """

        # Generate temp file for output
        tmp = np.memmap(tempfile.TemporaryFile(), dtype="float64", mode="w+", shape=(self.k, self.n))

        # Project and return data
        self.transMat = transMat[0:self.k,:]
        for i in range(0, self.n, self.m):
            tmp[:, i:i+self.m] = self.transMat.dot(self.data[:, i:i+self.m])

        return tmp


    def cov(self):
        """ Apply PCA using covariance matrix and eigenvectors.

        Returns
        -------
        2D numpy.core.memmap
            See project().

        """

        # Compute mean along each feature
        mean = np.zeros(self.m)
        for i in range(0, self.n, self.m):
            mean += np.sum(self.data[:, i:i+self.m], axis=1)
        mean /= self.n

        # Center data along features
        for i in range(0, self.n, self.m):
            self.data[:, i:i+self.m] -= mean[:, None]

        # Build covariance matrix, which is assumed to fit into memory
        covMat = np.zeros((self.m, self.m))
        for i in range(0, self.n, self.m):
            covMat += self.data[:, i:i+self.m].dot(self.data[:, i:i+self.m].T)

        # Normalize
        covMat *= (1. / (self.n - 1))

        # Determine (already sorted) eigenvalues and eigenvectors
        eigVal, eigVec = np.linalg.eigh(covMat)

        # Look at most important features
        self.k = presQual(eigVal[::-1], self.k, self.p)

        # Sort eigenvectors by eigenvalues
        eigVec = eigVec[:, eigVal.argsort()[::-1]]

        # Return data, which has been projected on k-first eigenvectors
        return self.project(eigVec.T)


def presQual(values, k, p):
    """ Given a certain quality assure the selected first-k features
        will preserve this quality. This adjusts self.k to a sufficient size.

    Parameters
    ----------
    values: numpy.ndarray
        Eigenvalues in descending order
    p: float
        Quality preservation in terms of (0,1]
    k: int
        Previous first-k features

    Returns
    -------
    int
        First-k features which satisfy the quality preservation requirement

    """

    x = (1. / np.sum(values))
    if (p > 0.):
        s = 0.
        for idx, v in enumerate(values):
            s += v
            if s * x > p:
                print "Quality preservation (", s * x, ") accomplished with k =", idx + 1
                return idx + 1
    else:
        print "First", k, "features reflect", np.sum(values[:k]) * x, "of information."
        return k


def pca(X, k, mode, p=0., sigma=0.5, useGpu=False, returnEigVec=False):
    """ Method for use with module.

    Parameters
    ----------
    X: 2D numpy.ndarray / numpy.core.memmap
        Data matrix of dimensions MxN with M features and N samples.
        It must hold that N >= M.
    k: int
        First k features with greatest variances data should be projected onto
    mode: str
        Implementation method for PCA. Valid modes: 'cov', 'svd', 'kernel'.
        Note: Only 'cov' is supported for numpy.core.memmap.
    p: float, optional, default: 0.
        Quality of information preservation in percent for use with mode="cov".
        If zero, it is ignored.
    sigma: float, optional, default: 0.5
        Standard deviation for RBF kernel function

    Returns
    -------
    2D numpy.ndarray / numpy.core.memmap
        Projected data of dimensions kxN
        Note: File underlying a numpy.core.memmap will be automatically removed
        on program termination.

    """

    # Exceptions in case of wrong parameters
    assert any([m == mode for m in ["cov", "svd", "kernel"]]), "Invalid mode. Valid ones are cov, svd, kernel"

    # Determine, which mode to use
    pc = PCA(X, k, p) if type(X) is np.ndarray else PCAMmap(X, k, p)
    if "cov" == mode:
        if returnEigVec:
            return pc.cov(), pc.transMat
        else:
            return pc.cov()
    elif "kernel" == mode:
        return pc.kPCA(sigma)
    else:
        return pc.svd()

