import numpy as np
import theano

class PCAGpu:
    """ Apply PCA for use with GPU via theano.

    Attributes
    ----------
    data: 2D numpy.ndarray
        Data matrix of dimensions MxN with M features and N samples.
        It must hold that N >= M.
    k: int
        k-first features data should be projected onto. It must hold that 0 < k <= M.
    mode: str
        Implementation method for PCA. Valid modes: 'cov', 'svd'.
    sigma: float, optional, default: 0.5
        Standard deviation for RBF kernel function

    """

    def __init__(self, X, k, mode, sigma=0.5):

        self.data = X
        self.k = k
        self.mode = mode
        self.sigma = sigma

        # Build solution using covariance matrix
        data = theano.tensor.fmatrix("Data")
        sc = theano.tensor.fscalar("Normalization factor")
        ks = theano.tensor.iscalar("k first features")
        cdata = data - theano.tensor.mean(data, axis=1)[:, None]
        cov = sc * cdata.dot(cdata.T)
        val, vec = theano.tensor.nlinalg.eigh(cov)
        vec = vec[:, val.argsort()[::-1]]
        pdata = vec.T[:ks,:].dot(cdata)
        self.pcaUsingCov = theano.function([data, ks, sc], pdata, allow_input_downcast=True)

        # Build solution using svd
        n = theano.tensor.iscalar("Number of samples - 1")
        y = cdata.T * (1. / theano.tensor.sqrt(n))
        transMat = theano.tensor.nlinalg.svd(y)[2]
        pdata = transMat[:ks,:].dot(cdata)
        self.pcaUsingSvd = theano.function([data, ks, n], pdata, allow_input_downcast=True)


    def pca(self):
        """ Make use of compiled function graphs. """

        if "cov" == self.mode:
            n = 1. / (self.data.shape[1] - 1)
            return self.pcaUsingCov(self.data, self.k, n)
        elif "svd" == self.mode:
            print "Warning: Theano's SVD class has yet to support gpu. Computation is done on CPU"
            n = self.data.shape[1] - 1
            return self.pcaUsingSvd(self.data, self.k, n)
        elif "kernel" == self.mode:
            print "Not yet implemented."
        else:
            raise PCAException("What!?")
