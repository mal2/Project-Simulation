﻿import pca
import numpy as np
import unittest
import numpy.testing as npt
import math


class TestPCA(unittest.TestCase):
    """ Test Suite for PCA module. """

    def test_init(self):
        testDataArray = np.array([[1, 4.5, 4, 7],
                                  [2, 7.3, 5, 8],
                                  [3, 1.2, 9, 9]])
        testK = 2

        testP = pca.PCA(testDataArray, testK)
        #testPB = pca.PCABig("testDataFile.csv", "transformed.bin", testK)

        # Test Array Equality
        npt.assert_array_max_ulp(testP.data, testDataArray, maxulp = 0.)
        #npt.assert_array_max_ulp(testPB.data, "testDataFile.csv", maxulp = 0.)

        # Test requested dimension equality
        self.assertTrue(testP.k == testK)
        #self.assertTrue(testPB.k == testK)

    def test_initAssertions(self):

        testingList = [1,2,3]
        testingString = "test"
        testingInteger = 1
        testingFloat = 1.5
        testSmallerDimArray = np.array([1,2,3])
        testRightDimArray = np.array([[1,2,3],
                                      [4,5,6],
                                      [7,8,9]])
        testSamplesSmallerThanFeatures = np.array([[1, 4],
                                                 [2, 5],
                                                 [3, 6]])
        testRightK = 1
        testHigherK = 4
        testNegativeK = -1


        # "X is not an numpy.ndarray." - Exception
        self.k = testRightK

        self.data = testingList
        self.assertFalse(type(self.data) is np.ndarray)
        self.assertRaises(AssertionError, lambda: pca.PCA(self.data, self.k))

        self.data = testingString
        self.assertFalse(type(self.data) is np.ndarray)
        self.assertRaises(AssertionError, lambda: pca.PCA(self.data, self.k))

        self.data = testingInteger
        self.assertFalse(type(self.data) is np.ndarray)
        self.assertRaises(AssertionError, lambda: pca.PCA(self.data, self.k))

        self.data = testingFloat
        self.assertFalse(type(self.data) is np.ndarray)
        self.assertRaises(AssertionError, lambda: pca.PCA(self.data, self.k))


        ## "k has to be greater than zero."
        self.data = testRightDimArray
        self.k = testNegativeK

        self.assertFalse(self.k > 1)
        self.assertRaises(AssertionError, lambda: pca.PCA(self.data, self.k))


        ## "k has to be an integer."
        self.data = testRightDimArray

        self.k = testingList
        self.assertFalse(type(self.k) is int)
        self.assertRaises(AssertionError, lambda: pca.PCA(self.data, self.k))

        self.k = testingString
        self.assertFalse(type(self.k) is int)
        self.assertRaises(AssertionError, lambda: pca.PCA(self.data, self.k))

        self.k = testRightDimArray
        self.assertFalse(type(self.k) is int)
        self.assertRaises(AssertionError, lambda: pca.PCA(self.data, self.k))

        #self.assertFalse(type("testRightDimMatrix.csv") is int)
        #self.assertRaises(AssertionError, lambda: pca.PCA(self.data, "testRightDimMatrix.csv"))

        self.k = testingFloat
        self.assertFalse(type(self.k) is int)
        self.assertRaises(AssertionError, lambda: pca.PCA(self.data, self.k))


        ## "X is not a 2-dimensional numpy.ndarray."
        self.k = testRightK
        self.data = testSmallerDimArray

        self.assertFalse(2 == len(self.data.shape))
        self.assertRaises(AssertionError, lambda: pca.PCA(self.data, self.k))

        #TODO


        ## "Number of samples is smaller than number of features."
        self.k = testRightK
        self.data = testSamplesSmallerThanFeatures

        self.dimensions = self.data.shape[0]
        self.samples = self.data.shape[1]
        self.assertTrue(self.samples < self.dimensions)
        self.assertRaises(AssertionError, lambda: pca.PCA(self.data, self.k))

        #TODO


        ## "Parameter k exceeds number of features."
        self.k = testHigherK
        self.data = testRightDimArray

        self.assertTrue(self.k > self.dimensions)
        self.assertRaises(AssertionError, lambda: pca.PCA(self.data, self.k))


    def test_fitCov(self):
        """ Test PCA using covariance matrix approach. """

        testRequestedDim = 1
        generatedDatasetsGaussian = 10

        # for covariance PCA
        testDataSet = np.array([[0, 1, 2, 3, 4, 0,  0],
                                [0, 0, 0, 0, 0, 1, -1]], dtype=np.float64)
        expRes = np.array([[-1.42857, -0.42857, 0.57143, 1.57143, 2.57143, -1.42857, -1.42857]]) # Beware the 2-dimensionality

        resCov = pca.pca(testDataSet, testRequestedDim, "cov")
        npt.assert_array_almost_equal(expRes, resCov)

        ## with random generated datas
        for i in range (generatedDatasetsGaussian):

            ## non memmap-version
            testDataSet = np.load("unitTest/testData/" + "testGaussianClasses" + str(i+1) + ".npy")
            expRes = np.load("unitTest/testData/" + "testGaussianClassesTransformed" + str(i+1) + ".npy")

            resCov = pca.pca(testDataSet, 1, "cov")
            npt.assert_array_almost_equal(expRes, resCov)

            ## memmap-version
            testDataSet = np.memmap("unitTest/testData/" + "testGaussianClassesMmap" + str(i+1) + ".npy", dtype="float64", mode="r+", shape=(2,2000))

            resCov = pca.pca(testDataSet, 1, "cov")
            npt.assert_array_almost_equal(expRes, resCov)



    def test_fitSvd(self):
        """ Test PCA using SVD approach. """

        testRequestedDim = 1
        generatedDatasetsGaussian = 5

        # for svd PCA
        testDataSet = np.array([[0, 1, 2, 3, 4, 0,  0],
                                [0, 0, 0, 0, 0, 1, -1]], dtype=np.float64)
        expRes = np.array([[-1.42857, -0.42857, 0.57143, 1.57143, 2.57143, -1.42857, -1.42857]]) # Beware the 2-dimensionality
        resSvd = pca.pca(testDataSet, testRequestedDim, "svd")
        npt.assert_array_almost_equal(expRes, resSvd)

        ## with random generated datas
        for i in range (generatedDatasetsGaussian):
            testDataSet = np.load("unitTest/testData/" + "testGaussianClasses" + str(i+1) + ".npy")
            expRes = np.load("unitTest/testData/" + "testGaussianClassesTransformed" + str(i+1) + ".npy")

            resSvd = pca.pca(testDataSet, 1, "svd")
            npt.assert_array_almost_equal(expRes, resSvd)


    def test_kPCA(self):
        generatedDatasetsCircle = 5

        # for kPCA with random generated datas

        for i in range (generatedDatasetsCircle):
            testDataSet = np.load("unitTest/testData/" + "testCircles" + str(i+1) + ".npy")
            expRes = np.load("unitTest/testData/" + "testCirclesTransformed" + str(i+1) + ".npy")

            resKpca = pca.pca(testDataSet, 1, "kernel")
            npt.assert_array_almost_equal(expRes, resKpca)


if __name__ == '__main__':
    unittest.main()
