import numpy as np
from Cluster import Dbscan
from PCA import pca


def fromFile(filename, length):
        with open(str(filename),'r') as f_handle:
            x = np.fromfile(f_handle, dtype=bool)
        system = np.reshape(x,(-1,length*length))
        return system


x = fromFile("Cluster/out_wo_header_16_2000.bin",16)
#x=x.T
print x.shape
minNeighbors = 15
epsilon = 1.01
data = pca.pca(x, 10,"svd")
data = np.array(data, dtype=float)
dbscanner = Dbscan(data, minNeighbors, epsilon)
clusterList, noise = dbscanner.run()
for cluster in clusterList:
    print cluster[0]