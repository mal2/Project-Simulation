﻿try:
    from setuptools import setup
    from setuptools import Extension
except ImportError:
    from distutils.core import setup
    from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy

modules=[
    Extension('cneighbor', sources=['fusim16/Cluster/cythondistance.pyx' , 'fusim16/Cluster/_neighbor.c']),
    Extension('cSystem', sources = ['fusim16/Ising/__clib__/cSystem.pyx', 'fusim16/Ising/__clib__/_cIsing.c'])              
]

setup(
    name = 'FUSim16',
    version = '0.1',
    description = 'Simulation for an Ising model with evaluation',
    long_description = 'In this module spins are simulated via the Ising model, reduced in dimension by an principal component analysis and clustered.',
    author = 'a, Kenneth Bartsch, Felicia Burtscher, Malte Detlefsen, Sandro Wrzalek, Arno Feiden, c and Frank Noe',
    url = 'https://github.com/Sawrz/Project-Simulation',
    download_url = 'https://github.com/Sawrz/Project-Simulation.git',
    include_dirs=[numpy.get_include()],  
#    package_dir = {'' : 'src'},
    ext_modules = modules,
#    packages = ['ising', 'ising.lib', 'pca', 'cluster'],
    packages = ['fusim16','fusim16.Ising','fusim16.Ising.__lib__', 'fusim16.PCA', 'fusim16.Cluster'],
    include_package_data=True,
    install_requires=['numpy','scipy', 'matplotlib'],
    cmdclass = {'build_ext': build_ext}
)